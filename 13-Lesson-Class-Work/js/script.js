// 1 ЗАДАНИЕ ПРАКТИКИ (При нажатии на кнопку отправки формы необходимо провести валидацию следующих полей:
// - Email не пустой
// - Пароль и подтверждение не пустые и совпадают
// - Поле ФИО содержат три элемента.)
$("#btnConfirm").click( () => {
    let arr = $("#fullName").val().split(' ');
    if($("#email").val() == "" || $("#email").val() == null){
        alert("Поле ввода Email пустое")
    }else if($("#email").val().indexOf("@") < 0){
        alert("Email должен содержать знак '@'");
    } else if($("#password").val() == "" || $("#password").val()== null){
        alert("Поле Password пустое")
    }else if($("#passwordAgain").val() == "" || $("#passwordAgain").val() == null){
        alert("Поле Password again пустое")
    }else if($("#password").val() !== $("#passwordAgain").val()){
        alert("Поле Password и Password again не совпадают")
    }else if($("#fullName").val() == "" || $("#fullName").val() == null){
        alert("Поле ввода ФИО пустое")
    }else if(arr.length !== 3){
        alert("Поле ввода ФИО заполнено не корректно")
    }else{
        alert("Спасибо, Ваши данные приняты в обработку")
        $("#form1")[0].reset();
    }
});

//2 ЗАДАНИЕ ПРАКТИКИ (Реализуем обработку формы “Обратный звонок” по нажатию на кнопку.)
$("#btnCallback").click( () => {$(".form2").css("display","block")});
$("#buttonCallback").click(() => {
    if($("#name").val() !== '' && $("#phone").val() !== ''){
        alert("Менеджер скоро свяжется с Вами");
        $("#form2").css("display","none");
        $("#form2")[0].reset();
    }else {
        alert("Заполните пожалуйста все поля")
    }
})

//3 ЗАДАНИЕ ПРАКТИКИ (Фиксация меню при прокрутке)
window.onscroll = () => { $('#header').css("top", '0') }