//1 ЗАДАНИЕ ПРАКТИКИ (При нажатии на кнопку отправки формы необходимо провести валидацию следующих полей:
// - Email не пустой
// - Пароль и подтверждение не пустые и совпадают
// - Поле ФИО содержат три элемента.)
let email = document.getElementById("email");
let pas = document.getElementById("password");
let pasAgain = document.getElementById("passwordAgain");
let fullName = document.getElementById("fullName");

let form1 = document.getElementById("form1")
let btnConfirm = document.getElementById("btnConfirm");
btnConfirm.addEventListener('click', validForm)

function validForm() {
    let arr = fullName.value.split(' ');
    if(email.value == "" || email.value == null){
        alert("Поле ввода Email пустое")
    }else if(pas.value == "" || pas.value == null){
        alert("Поле Password пустое")
    }else if(pasAgain.value == "" || pasAgain.value == null){
        alert("Поле Password again пустое")
    }else if(pas.value !== pasAgain.value){
        alert("Поле Password и Password again не совпадают")
    }else if(fullName.value == "" || fullName.value == null){
        alert("Поле ввода ФИО пустое")
    }else if(arr.length !== 3){
        alert("Поле ввода ФИО заполнено не корректно")
    }else{
        alert("Спасибо, Ваши данные приняты в обработку")
    }
    form1.reset();
}

//2 ЗАДАНИЕ ПРАКТИКИ (Реализуем обработку формы “Обратный звонок” по нажатию на кнопку.)
let btn_call_back = document.getElementById("btnCallback")
btn_call_back.addEventListener("click", showFormCallBack)
let showForm = document.querySelector(".form2")
function showFormCallBack(){
    showForm.style.display="block"
}

let name = document.getElementById("name");
let phone = document.getElementById("phone");
let btn_call = document.getElementById("buttonCallback")
btn_call.addEventListener('click', callBackMe)
function callBackMe(){
    if(name.value !== '' && phone.value !== ''){
        alert("Менеджер скоро свяжется с Вами");
        showForm.style.display="none";
        showForm.reset();
    }else {
        alert("Заполните пожалуйста все поля")
    }
}

//3 ЗАДАНИЕ ПРАКТИКИ (Фиксация меню при прокрутке)
window.onscroll = function showHeader(){
    let elem = document.getElementById('header');
    if(window.pageYOffset > 280){
        elem.style.top='0';
    }
}