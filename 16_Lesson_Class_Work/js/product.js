'use strict'
class Product {

    constructor(title, description, price, salePrice, url){
        this.title = title;
        this.description = description;
        this.price=price;
        this.salePrice=salePrice;
        this.url=url;
    }

    setTitle(title) {
        this.title = title;
    }
    getTitle() {
        return this.title;
    }
    showTitle() {
        console.log(this.title)
    }

    setDescription (description){
        this.description = description;
    }
    getDescription(){
        return this.description;
    }
    showDescription (){
        console.log(this.description)
    }

    setPrice (price){
        this.price = price;
    }
    getPrice(){
        return this.price;
    }
    showPrice (){
        console.log(this.price)
    }

    setSalePrice (salePrice){
        this.salePrice = salePrice;
    }
    getSalePrice(){
        return this.salePrice;
    }
    showSalePrice (){
        console.log(this.salePrice)
    }
    setUrl (url){
        this.url = url;
    }
    getUrl(){
        return this.url;
    }
    showUrl (){
        console.log(this.url)
    }
}