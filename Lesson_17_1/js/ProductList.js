'use strict'
class ProductList extends Component{
    constructor(id, title) {
        super(id, title);

        this.products=[];
    }
    render() {
        super.render();

        for (let product of this.products) {
            product.render()
        }
        $(".wrap1, .wrap2, .wrap3, .wrap4, .wrap5, .wrap6, .wrap7, .wrap8").wrapAll('<div class="inner">');
    }
    addProduct(product) {
        product.setParent(this.id)
        this.products.push(product);
    }
}
