'use strict'
class Product extends Component{
    constructor(id, title, url, price) {
        super(id, title);
        this.url = url;
        this.price = price;
        this.parentId = null;
    }

    setParent(parentId){
        this.parentId = parentId;
    }

    render() {
        $(`${"#" + this.parentId}`).append(
            "<div class=" + "wrap" + this.id +">" +
            "<h2>" + this.title + "</h2>" +
            "<img src="+ this.url +">"+
            "<h2>" +"Цена: " + this.price +" грн" + "</h2>" +
            "<button>Купить</button>" +
            "</div>");

    }

}