'use strict'
class Component {
    constructor(id, title) {
        this.id = id;
        this.title = title;
    }

    render() {
        $(`${"#" + this.id}`).append(`<h1>${this.title}</h1>`);
    }
}

