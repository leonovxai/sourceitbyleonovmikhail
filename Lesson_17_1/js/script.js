let category = new Category("div1", "Продукты");
category.render();

let productList = new ProductList("div2", "Список продуктов");

productList.addProduct(new Product(1, "Хлеб", "https://picsum.photos/200", 200));
productList.addProduct(new Product(2, "Булка", "https://picsum.photos/200",200));
productList.addProduct(new Product(3, "Батон", "https://picsum.photos/200",200));
productList.addProduct(new Product(4, "Бублики", "https://picsum.photos/200",200));
productList.addProduct(new Product(5, "Рогалики", "https://picsum.photos/200",200));
productList.addProduct(new Product(6, "Пряники", "https://picsum.photos/200",200));
productList.addProduct(new Product(7, "Пироженое", "https://picsum.photos/200",200));
productList.addProduct(new Product(8, "Сухарики", "https://picsum.photos/200",200));

productList.render();

