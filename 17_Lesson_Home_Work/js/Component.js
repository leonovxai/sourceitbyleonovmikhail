'use strict'
class Component {
    constructor(id, title) {
        this.id = id;
        this.title = title;
    }

    render() {
        // $(`${"#" + this.id}`).append("<div><h1>"+this.title+"</h1></div>");
        $(`${"#" + this.id}`).append(`<div id="${this.title}"><h1>${this.title}</h1></div>`);
    }
}

