let category = new Category("div1", "Categories");
category.render();
category.addCategory("Бакалея");
category.addCategory("Рыбный отдел");
category.addCategory("Канцелярия");
category.addCategory("Хозтовары");
category.addCategory("Мясной отдел");
category.addCategory("Вино-водочный отдел");
category.addCategory("Сигаретный отдел");
let productList = new ProductList("div1", "Product_list");
productList.render();
productList.addProduct("Хлеб");
productList.addProduct("Булка");
productList.addProduct("Батон");
productList.addProduct("Бублики");
productList.addProduct("Рогалики");
productList.addProduct("Пряники");
productList.addProduct("Пироженое");
productList.addProduct("Сухарики");

//  в классе категори метод add, чтоб добавлять категории .add("хлеб")
//  в классе продукт метод add, чтоб добавлять продукты в массив .add("хлеб")