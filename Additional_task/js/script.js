let productList = [
    ["Ноутбук SAMSUNG", "15000 грн.", 'https://nure.ua/wp-content/uploads/2018/04/samsung-electronics-logo.jpg'],
    ["Ноутбук LENOVO", "13500 грн.", 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYGFMCxiRqEd0OdlUvfGMArLw-Nx9hSo8bx9zmA-TBZRymLXQhseH7K7Pm0VbsFKm4Mbo&usqp=CAU'],
    ["Ноутбук XIAOMI", "16700 грн.", 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Xiaomi_logo.svg/2048px-Xiaomi_logo.svg.png'],
    ["Ноутбук LG", "14300 грн.", 'https://e7.pngegg.com/pngimages/305/107/png-clipart-lg-electronics-led-backlit-lcd-computer-monitors-home-appliance-lg-text-trademark.png'],
    ["Смартфон XIAOMI", "10900 грн.", 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Xiaomi_logo.svg/2048px-Xiaomi_logo.svg.png'],
    ["Смартфон SAMSUNG", "12100 грн.", 'https://nure.ua/wp-content/uploads/2018/04/samsung-electronics-logo.jpg'],
    ["Смартфон LENOVO", "9500 грн.", 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYGFMCxiRqEd0OdlUvfGMArLw-Nx9hSo8bx9zmA-TBZRymLXQhseH7K7Pm0VbsFKm4Mbo&usqp=CAU'],
    ["Смартфон SONY", "8900 грн.", 'https://www.cgiainstitute.org/wp-content/uploads/2020/05/Sony_CGIA.jpg'],
    ["Телевизор SAMSUNG", "28400 грн.", 'https://nure.ua/wp-content/uploads/2018/04/samsung-electronics-logo.jpg'],
    ["Телевизор LG", "27300 грн.", 'https://e7.pngegg.com/pngimages/305/107/png-clipart-lg-electronics-led-backlit-lcd-computer-monitors-home-appliance-lg-text-trademark.png'],
    ["Телевизор SONY", "29500 грн.", 'https://www.cgiainstitute.org/wp-content/uploads/2020/05/Sony_CGIA.jpg'],
    ["Телевизор TOSHiBA", "29800 грн.", 'https://e7.pngegg.com/pngimages/508/471/png-clipart-laptop-toshiba-satellite-toshiba-portege-electronics-laptop-electronics-text.png'],
    ["Планшет SAMSUNG", "8300 грн.",'https://nure.ua/wp-content/uploads/2018/04/samsung-electronics-logo.jpg'],
    ["Планшет LENOVO", "7900 грн.", 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYGFMCxiRqEd0OdlUvfGMArLw-Nx9hSo8bx9zmA-TBZRymLXQhseH7K7Pm0VbsFKm4Mbo&usqp=CAU'],
    ["Планшет XIAOMI", "8000 грн.", 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Xiaomi_logo.svg/2048px-Xiaomi_logo.svg.png'],
    ["Планшет SONY", "9500 грн.", "https://www.cgiainstitute.org/wp-content/uploads/2020/05/Sony_CGIA.jpg"]
]
$("#addProd").click(() => {
    $("#formAddProd").css("display", "block");
})

$("#btn_add_prod").click(() => {
    if($("#prodName").val() == " " || $("#prodName").val() == null || $("#prodName").val() == "" ) {
        alert("Введите название товара");
    }else if($("#priceProd").val() == " " || $("#priceProd").val() == null || $("#priceProd").val() == "") {
        alert("Введите цену товара");
    }else if($("#imgProd").val() == " " || $("#imgProd").val() == null || $("#imgProd").val() == ""){
        alert("Добавьте изображение товара");
    }else{
        alert("ТОВАР ДОБАВЛЕН!");
        productList.push([$("#prodName").val(),$("#priceProd").val(),$("#imgProd").val()]);
        $("#formAddProd").css("display", "none");
        $("#form")[0].reset();
    }
})

$(document).mouseup(function (e){
    let div = $("#formAddProd");
    if (!div.is(e.target) && div.has(e.target).length === 0) {
        div.hide();
    }
});

$("#listProd").click( () => {
    $(".inner_catalog").css("display", "flex");
    for (let i = 0; i < productList.length; i++) {
        $(".inner_catalog").append("<div class='inner_position position-" + i + "'>" +
            "<img alt='photo' width='280px' height='350px'><br>" +
            "<span ></span><br><p ></p></div>");

        $(".inner_position.position-" + i + " > img").attr('src', productList[i][2])
        $(".inner_position.position-" + i + " > span").html(productList[i][0])
        $(".inner_position.position-" + i + " > p").html(productList[i][1])
    }
})
$(document).mouseup(function (e){
    let div = $(".inner_catalog");
    if (!div.is(e.target) && div.has(e.target).length === 0) {
        div.hide();
    }
    $(".inner_catalog").empty()
});







